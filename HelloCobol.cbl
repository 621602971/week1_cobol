      *****************************************************************
      * Program name:    HELLO-COBOL                               
      * Original author: MYNAME                                
      *
      * Maintenence Log                                              
      * Date      Author        Maintenance Requirement               
      * --------- ------------  --------------------------------------- 
      * 01/01/08 MYNAME  Created for COBOL class         
      *                                                               
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  HELLO-COBOL.
       DATA DIVISION. 
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY "HELLO COBOL".
       END PROGRAM HELLO-COBOL.
      *****************************************************************
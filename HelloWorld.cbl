      *****************************************************************
      * Program name:    MYPROG                               
      * Original author: MYNAME                                
      *
      * Maintenence Log                                              
      * Date      Author        Maintenance Requirement               
      * --------- ------------  --------------------------------------- 
      * 01/01/08 MYNAME  Created for COBOL class         
      *                                                               
      *****************************************************************
       IDENTIFICATION DIVISION.
       PROGRAM-ID.  HELLO-WORLD.
       DATA DIVISION. 
       PROCEDURE DIVISION.
       MAIN-PROCEDURE.
           DISPLAY  "HELLO, WORLD".
       END PROGRAM HELLO-WORLD.
      *****************************************************************